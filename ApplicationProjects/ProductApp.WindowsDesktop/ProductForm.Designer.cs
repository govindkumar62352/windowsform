﻿namespace ProductApp.WindowsDesktop
{
    partial class ProductForm
    {
        /// <summary>
        ///  Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        ///  Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        ///  Required method for Designer support - do not modify
        ///  the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblProductName = new System.Windows.Forms.Label();
            this.lblProductDescription = new System.Windows.Forms.Label();
            this.lblProductIsAvailable = new System.Windows.Forms.Label();
            this.txtProductName = new System.Windows.Forms.TextBox();
            this.txtProductDescription = new System.Windows.Forms.TextBox();
            this.dropdownProductIsAvailable = new System.Windows.Forms.DomainUpDown();
            this.btnSubmit = new System.Windows.Forms.Button();
            this.btnCancel = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // lblProductName
            // 
            this.lblProductName.AutoSize = true;
            this.lblProductName.Location = new System.Drawing.Point(164, 75);
            this.lblProductName.Name = "lblProductName";
            this.lblProductName.Size = new System.Drawing.Size(59, 25);
            this.lblProductName.TabIndex = 0;
            this.lblProductName.Text = "Name";
            this.lblProductName.Click += new System.EventHandler(this.lblProductName_Click);
            // 
            // lblProductDescription
            // 
            this.lblProductDescription.AutoSize = true;
            this.lblProductDescription.Location = new System.Drawing.Point(164, 134);
            this.lblProductDescription.Name = "lblProductDescription";
            this.lblProductDescription.Size = new System.Drawing.Size(102, 25);
            this.lblProductDescription.TabIndex = 1;
            this.lblProductDescription.Text = "Description";
            // 
            // lblProductIsAvailable
            // 
            this.lblProductIsAvailable.AutoSize = true;
            this.lblProductIsAvailable.Location = new System.Drawing.Point(164, 199);
            this.lblProductIsAvailable.Name = "lblProductIsAvailable";
            this.lblProductIsAvailable.Size = new System.Drawing.Size(95, 25);
            this.lblProductIsAvailable.TabIndex = 2;
            this.lblProductIsAvailable.Text = "isAvailable";
            // 
            // txtProductName
            // 
            this.txtProductName.Location = new System.Drawing.Point(348, 69);
            this.txtProductName.Name = "txtProductName";
            this.txtProductName.Size = new System.Drawing.Size(180, 31);
            this.txtProductName.TabIndex = 3;
            // 
            // txtProductDescription
            // 
            this.txtProductDescription.Location = new System.Drawing.Point(348, 134);
            this.txtProductDescription.Name = "txtProductDescription";
            this.txtProductDescription.Size = new System.Drawing.Size(180, 31);
            this.txtProductDescription.TabIndex = 4;
            // 
            // dropdownProductIsAvailable
            // 
            this.dropdownProductIsAvailable.Items.Add("True");
            this.dropdownProductIsAvailable.Items.Add("False");
            this.dropdownProductIsAvailable.Location = new System.Drawing.Point(348, 199);
            this.dropdownProductIsAvailable.Name = "dropdownProductIsAvailable";
            this.dropdownProductIsAvailable.Size = new System.Drawing.Size(180, 31);
            this.dropdownProductIsAvailable.TabIndex = 5;
            this.dropdownProductIsAvailable.Text = "Product Availability";
            // 
            // btnSubmit
            // 
            this.btnSubmit.Location = new System.Drawing.Point(234, 280);
            this.btnSubmit.Name = "btnSubmit";
            this.btnSubmit.Size = new System.Drawing.Size(112, 34);
            this.btnSubmit.TabIndex = 6;
            this.btnSubmit.Text = "Submit";
            this.btnSubmit.UseVisualStyleBackColor = true;
            this.btnSubmit.Click += new System.EventHandler(this.btnSubmit_Click);
            // 
            // btnCancel
            // 
            this.btnCancel.Location = new System.Drawing.Point(416, 280);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(112, 34);
            this.btnCancel.TabIndex = 7;
            this.btnCancel.Text = "Cancel";
            this.btnCancel.UseVisualStyleBackColor = true;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // ProductForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(10F, 25F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.ClientSize = new System.Drawing.Size(873, 520);
            this.ControlBox = false;
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.btnSubmit);
            this.Controls.Add(this.dropdownProductIsAvailable);
            this.Controls.Add(this.txtProductDescription);
            this.Controls.Add(this.txtProductName);
            this.Controls.Add(this.lblProductIsAvailable);
            this.Controls.Add(this.lblProductDescription);
            this.Controls.Add(this.lblProductName);
            this.ForeColor = System.Drawing.SystemColors.InactiveCaptionText;
            this.Name = "ProductForm";
            this.Text = "ProductApp";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private Label lblProductName;
        private Label lblProductDescription;
        private Label lblProductIsAvailable;
        private TextBox txtProductName;
        private TextBox txtProductDescription;
        private DomainUpDown dropdownProductIsAvailable;
        private Button btnSubmit;
        private Button btnCancel;
    }
}