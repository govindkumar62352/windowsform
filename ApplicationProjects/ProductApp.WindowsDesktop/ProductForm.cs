using ProductApp.WindowsDesktop.Models;

namespace ProductApp.WindowsDesktop
{
    public partial class ProductForm : Form
    {
        static int counter = 0;
        public ProductForm()
        {
            InitializeComponent();
        }

        private void lblProductName_Click(object sender, EventArgs e)
        {

        }

        private void btnSubmit_Click(object sender, EventArgs e)
        {
            Product product = new Product()
            {
                Id = counter + 1,
                Name = txtProductName.Text,
                Description = txtProductDescription.Text,
                isAvailable = Convert.ToBoolean(dropdownProductIsAvailable.Text)
            };
            counter = counter + 1;
            MessageBox.Show($"{product}","ProductApp",MessageBoxButtons.OKCancel,MessageBoxIcon.Information);
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}